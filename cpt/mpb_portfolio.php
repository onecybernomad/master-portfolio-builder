<?php 

//================================= ADD CUSTOM POST TYPES ======================== 
add_action( 'init', 'cptui_register_my_cpts' );
function cptui_register_my_cpts() {
	$labels = array(
		"name" => "Portfolio items",
		"singular_name" => "Portfolio",
		"menu_name" => "Portfolio items",
		);

	$args = array(
		"labels" => $labels,
		"description" => "Add Items to your portfolio page by adding individual portfolio items to this post type.",
		"public" => true,
		"show_ui" => true,
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "portfolio", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "http://codesource.tech/wp-content/uploads/2015/10/portfolio-512-e1444862023414.png",		
		"supports" => array( "title", "custom-fields" ),		
	);
	register_post_type( "portfolio", $args );

// End of cptui_register_my_cpts()
}

// ============================ ADD CUSTOM FIELDS ==============================
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_portfolio-item',
		'title' => 'Portfolio item',
		'fields' => array (
			array (
				'key' => 'field_5626bf73bbfe8',
				'label' => 'Portfolio Title',
				'name' => 'portfolio_title',
				'type' => 'text',
				'instructions' => 'Type a title for your portfolio item.',
				'required' => 1,
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5620591a66e17',
				'label' => 'Link',
				'name' => 'link',
				'type' => 'text',
				'instructions' => 'type out the div in the Show and hide functions.',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_561973d273693',
				'label' => 'Portfolio Image',
				'name' => 'portfolio_image',
				'type' => 'image',
				'instructions' => 'Choose an image for your portfolio item.',
				'required' => 1,
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_5626e8e4a578a',
				'label' => 'Portfolio Image2',
				'name' => 'portfolio_image2',
				'type' => 'image',
				'instructions' => 'Choose an image for your portfolio item in the modal window.',
				'save_format' => 'object',
				'preview_size' => 'thumbnail',
				'library' => 'all',
			),
			array (
				'key' => 'field_5626e73f4fc8e',
				'label' => 'Portfolio Video',
				'name' => 'portfolio_video',
				'type' => 'text',
				'instructions' => 'Paste your embed code here.',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'html',
				'maxlength' => '',
			),
			array (
				'key' => 'field_5619742b73694',
				'label' => 'Portfolio Page',
				'name' => 'portfolio_page',
				'type' => 'wysiwyg',
				'default_value' => '',
				'toolbar' => 'full',
				'media_upload' => 'yes',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'portfolio',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
				0 => 'permalink',
				1 => 'the_content',
				2 => 'excerpt',
				3 => 'custom_fields',
				4 => 'discussion',
				5 => 'comments',
				6 => 'revisions',
				7 => 'slug',
				8 => 'author',
				9 => 'format',
				10 => 'featured_image',
				11 => 'categories',
				12 => 'tags',
				13 => 'send-trackbacks',
			),
		),
		'menu_order' => 0,
	));
}

