Synopsis

The absolute easiest and best plug-in to rapidly build stylish portfolios for your site! Quickly add your portfolio items to your portfolio and publish them to your portfolio. Select your options for style and layout and add it to your page or template using a handy shortcode.

Motivation

Sick of trying to find a good plug-in to set up portfolios on pages I was working on, I set out to write my own. 

Installation

Simply download or clone the project into your WordPress plug-ins directory or install from the WordPress plug-in repository. Go to plug-ins in your dashboard and activate the plug-in. Add portfolio items, style as you like them, publish, and add the shortcode to the page or template you want the portfolio added to. 

[shortcode]

Contributors

Jeremy Hayes URL: http://codesource.tech


License

    Master Portfolio Builder is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3.

    Master Portfolio Builder is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
